import configparser
import os
import socket
import getpass
import pathlib
from Lynx import Utils


class Options():
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.optionxform = str
        self.config_file = f"{str(pathlib.Path.home())}/.config/lynx/desktop.conf"
        self.config.read(self.config_file)

        ## Si el directorio no existe lo crea
        try:
            os.stat(f"{str(pathlib.Path.home())}/.config/lynx/")
        except:
            os.mkdir(f"{str(pathlib.Path.home())}/.config/lynx/")

    def save(self, key, value):
        self.config.set('DEFAULT', f'{key}', f'{value}')
        with open(self.config_file, 'w') as f:
            self.config.write(f)

    def loadVideos(self):
        dir = f"{str(pathlib.Path.home())}/.config/lynx/background/"
        contenido = os.listdir(dir)
        videos = []
        for file in contenido:
            if os.path.isfile(os.path.join(dir, file)) and file.endswith('.mp4'):
                videos.append(file)
        return videos

    def loadBanners(self):
        dir = f"{str(pathlib.Path.home())}/.config/lynx/banners/"
        contenido = os.listdir(dir)
        banners = []
        for file in contenido:
            if os.path.isfile(os.path.join(dir, file)) and (file.endswith('.jpg') or file.endswith('.png') or file.endswith('.gif') or file.endswith('.webp') or file.endswith('.jpeg')):
                banners.append(file)
        return banners

    def load(self):
        defaults = {
            "debug": False,
            "autoTile": False,
            "moodBackgroundBool": False,
            "moodBackgroundIMG":"",
            "backgroundVideos": self.loadVideos(),
            "Accessories": True,
            "Development": True,
            "Education": True,
            "Gaming": True,
            "GoOnline": True,
            "Graphics": True,
            "Help": True,
            "Multimedia": True,
            "Office": True,
            "System": True,
            "workspace1": "",
            "workspace2": "",
            "workspace3": "",
            "workspace4": "",
            "homeDir": f"{str(pathlib.Path.home())}/",
            "userName": f"{getpass.getuser()}",
            "hostName": f"{socket.gethostname()}",
            "bannersUsers": self.loadBanners(),
            "bannerIMG":"",
            "styleDarkMode": True,
            "styleBorderRadius": 18,
            "styleBlur": 6,
            "styleColor": "",
        }

        if os.path.exists(self.config_file):
            for key, value in self.config.items('DEFAULT'):
                if value == "true" or value == "false":
                    defaults[key] = self.config.getboolean('DEFAULT', key)
                else:
                    defaults[key] = value
        return defaults
