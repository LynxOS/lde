# Lynx Desktop Enviroment

## Características

* Interfaz limpia y mínima
* Panel de configuración
* Mostrar / ocultar categorías de aplicaciones
* Búsqueda de aplicaciones
* Búsqueda visual de aplicaciones
* El panel de configuración está integrado con la configuración de la aplicación individual
* Integración Dbus
* Inspector de UI
* Fondo Animado
* Azulejo automático de ventana opcional
* Espacio de trabajo programable
* Configuracion de Estetica
* Configuracion de Panel de Usuario
* Blur

### Prerequisitos

* Menu Data
* LDK
* Python GObject Introspection
* Python-Dbus
* Gnome Menu GObject Introspection

## License

* This project is licensed under the GPL License - see the [LICENSE.md](LICENSE.md) file for details.
* Background images are under Creative Commons Zero license.
